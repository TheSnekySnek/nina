﻿<!--
    Copyright © 2016 - 2019 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.TelescopeView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:rules="clr-namespace:NINA.Utility.ValidationRules"
    xmlns:util="clr-namespace:NINA.Utility"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition />
            <ColumnDefinition />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition />
            <RowDefinition />
        </Grid.RowDefinitions>
        <Grid Grid.ColumnSpan="1">

            <GroupBox>
                <GroupBox.Header>
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="3*" />
                        </Grid.ColumnDefinitions>
                        <TextBlock
                            VerticalAlignment="Center"
                            FontSize="20"
                            Text="{ns:Loc LblTelescope}" />
                        <Grid Grid.Column="1" Margin="5">
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="40" />
                                <ColumnDefinition />
                                <ColumnDefinition Width="Auto" />
                                <ColumnDefinition Width="Auto" />
                                <ColumnDefinition Width="Auto" />
                            </Grid.ColumnDefinitions>
                            <ninactrl:LoadingControl
                                Height="30"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                LoadingImageBrush="{StaticResource PrimaryBrush}"
                                Visibility="{Binding ChooseTelescopeCommand.Execution.IsNotCompleted, Converter={StaticResource BooleanToVisibilityCollapsedConverter}, FallbackValue=Collapsed}" />
                            <ComboBox
                                Grid.Column="1"
                                MinHeight="40"
                                DisplayMemberPath="Name"
                                IsEnabled="{Binding TelescopeInfo.Connected, Converter={StaticResource InverseBooleanConverter}}"
                                ItemsSource="{Binding TelescopeChooserVM.Devices}"
                                SelectedItem="{Binding TelescopeChooserVM.SelectedDevice}"
                                SelectedValuePath="Name" />
                            <Button
                                Grid.Column="2"
                                Width="40"
                                Height="40"
                                Margin="1,0,0,0"
                                Command="{Binding TelescopeChooserVM.SetupDialogCommand}"
                                IsEnabled="{Binding TelescopeChooserVM.SelectedDevice.HasSetupDialog}">
                                <Button.ToolTip>
                                    <ToolTip ToolTipService.ShowOnDisabled="False">
                                        <TextBlock Text="{ns:Loc LblSettings}" />
                                    </ToolTip>
                                </Button.ToolTip>
                                <Grid>
                                    <Path
                                        Margin="5"
                                        Data="{StaticResource SettingsSVG}"
                                        Fill="{StaticResource ButtonForegroundBrush}"
                                        Stretch="Uniform" />
                                </Grid>
                            </Button>
                            <Button
                                Grid.Column="3"
                                Width="40"
                                Height="40"
                                Margin="1,0,0,0"
                                Command="{Binding RefreshTelescopeListCommand}">
                                <Button.ToolTip>
                                    <ToolTip ToolTipService.ShowOnDisabled="False">
                                        <TextBlock Text="{ns:Loc LblRescanDevices}" />
                                    </ToolTip>
                                </Button.ToolTip>
                                <Grid>
                                    <Path
                                        Margin="5"
                                        Data="{StaticResource LoopSVG}"
                                        Fill="{StaticResource ButtonForegroundBrush}"
                                        Stretch="Uniform" />
                                </Grid>
                            </Button>
                            <Grid
                                Grid.Column="4"
                                Width="40"
                                Height="40"
                                Margin="1,0,0,0">
                                <ninactrl:CancellableButton
                                    ButtonForegroundBrush="{StaticResource ButtonForegroundDisabledBrush}"
                                    ButtonImage="{StaticResource PowerSVG}"
                                    CancelButtonImage="{StaticResource CancelSVG}"
                                    CancelCommand="{Binding CancelChooseTelescopeCommand}"
                                    Command="{Binding ChooseTelescopeCommand}"
                                    IsEnabled="{Binding TelescopeInfo.Connected, Converter={StaticResource InverseBooleanConverter}}"
                                    ToolTip="{ns:Loc LblConnect}"
                                    Visibility="{Binding TelescopeInfo.Connected, Converter={StaticResource InverseBoolToVisibilityConverter}}" />
                                <Grid Visibility="{Binding Telescope, Converter={StaticResource NullToVisibilityConverter}}">
                                    <Button
                                        Command="{Binding DisconnectCommand}"
                                        IsEnabled="{Binding TelescopeInfo.Connected}"
                                        Visibility="{Binding TelescopeInfo.Connected, Converter={StaticResource VisibilityConverter}}">
                                        <Button.ToolTip>
                                            <ToolTip ToolTipService.ShowOnDisabled="False">
                                                <TextBlock Text="{ns:Loc LblDisconnect}" />
                                            </ToolTip>
                                        </Button.ToolTip>
                                        <Grid>
                                            <Path
                                                Margin="5"
                                                Data="{StaticResource PowerSVG}"
                                                Fill="{StaticResource ButtonForegroundBrush}"
                                                Stretch="Uniform" />
                                        </Grid>
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </GroupBox.Header>
                <Grid>
                    <StackPanel>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblName}" />
                                    <TextBlock
                                        Margin="5,0,0,0"
                                        Text="{Binding Telescope.Name}"
                                        TextWrapping="WrapWithOverflow" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblDescription}" />
                                    <TextBlock
                                        Margin="5,0,0,0"
                                        Text="{Binding Telescope.Description}"
                                        TextWrapping="WrapWithOverflow" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblDriverInfo}" />
                                    <TextBlock
                                        Margin="5,0,0,0"
                                        Text="{Binding Telescope.DriverInfo}"
                                        TextWrapping="WrapWithOverflow" />
                                </UniformGrid>
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblDriverVersion}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding Telescope.DriverVersion}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblSiteLatitude}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding Telescope.SiteLatitude}" />
                                </UniformGrid>
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblSiteLongitude}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding Telescope.SiteLongitude}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblSiteElevation}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding Telescope.SiteElevation}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblSiderealTime}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding TelescopeInfo.SiderealTimeString}" />
                                </UniformGrid>
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblMeridianIn}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding TelescopeInfo.HoursToMeridianString}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblRightAscension}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding TelescopeInfo.RightAscensionString}" />
                                </UniformGrid>
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblDeclination}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding TelescopeInfo.DeclinationString}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblAltitude}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding TelescopeInfo.AltitudeString}" />
                                </UniformGrid>
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock Text="{ns:Loc LblAzimuth}" />
                                    <TextBlock Margin="5,0,0,0" Text="{Binding TelescopeInfo.AzimuthString}" />
                                </UniformGrid>
                            </UniformGrid>
                        </Border>
                    </StackPanel>
                </Grid>
            </GroupBox>
        </Grid>

        <Grid Grid.Row="1" IsEnabled="{Binding Telescope, Converter={StaticResource InverseNullToBooleanConverter}}">
            <Grid IsEnabled="{Binding TelescopeInfo.Connected}">
                <GroupBox Header="{ns:Loc LblManualCoordinates}">
                    <StackPanel>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <StackPanel Margin="0,6,0,6" Orientation="Horizontal">
                                <TextBlock Width="60" Text="{ns:Loc LblTargetRA}" />
                                <TextBox
                                    MinWidth="40"
                                    Margin="5,0,0,0"
                                    TextAlignment="Right">
                                    <TextBox.Text>
                                        <Binding Path="TargetRightAscencionHours" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:HoursRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>
                                <TextBlock>h</TextBlock>
                                <TextBox
                                    MinWidth="40"
                                    Margin="5,0,0,0"
                                    TextAlignment="Right">
                                    <TextBox.Text>
                                        <Binding Path="TargetRightAscencionMinutes" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:MinutesRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>
                                <TextBlock>m</TextBlock>
                                <TextBox
                                    MinWidth="40"
                                    Margin="5,0,0,0"
                                    TextAlignment="Right">
                                    <TextBox.Text>
                                        <Binding Path="TargetRightAscencionSeconds" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:SecondsRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>
                                <TextBlock>s</TextBlock>
                            </StackPanel>
                        </Border>
                        <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                            <StackPanel Margin="0,6,0,6" Orientation="Horizontal">
                                <TextBlock Width="60" Text="{ns:Loc LblTargetDec}" />
                                <TextBox
                                    MinWidth="40"
                                    Margin="5,0,0,0"
                                    TextAlignment="Right">
                                    <TextBox.Text>
                                        <Binding Path="TargetDeclinationDegrees" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:DegreesRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>
                                <TextBlock>d</TextBlock>
                                <TextBox
                                    MinWidth="40"
                                    Margin="5,0,0,0"
                                    TextAlignment="Right">
                                    <TextBox.Text>
                                        <Binding Path="TargetDeclinationMinutes" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:MinutesRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>
                                <TextBlock>m</TextBlock>
                                <TextBox
                                    MinWidth="40"
                                    Margin="5,0,0,0"
                                    TextAlignment="Right">
                                    <TextBox.Text>
                                        <Binding Path="TargetDeclinationSeconds" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:SecondsRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>
                                <TextBlock>s</TextBlock>
                            </StackPanel>
                        </Border>
                        <Button
                            Width="50"
                            Height="50"
                            Margin="5"
                            HorizontalAlignment="Left"
                            Command="{Binding SlewToCoordinatesCommand}">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblSlew}" />
                        </Button>
                    </StackPanel>
                </GroupBox>
            </Grid>
        </Grid>

        <Grid
            Grid.Row="1"
            Grid.Column="1"
            IsEnabled="{Binding Telescope, Converter={StaticResource InverseNullToBooleanConverter}}">
            <Grid IsEnabled="{Binding TelescopeInfo.Connected}">
                <GroupBox
                    Grid.Row="1"
                    Header="{ns:Loc LblManualControl}"
                    IsEnabled="{Binding Telescope.CanSlew}">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="100" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="*" />
                            <RowDefinition Height="100" />
                            <RowDefinition Height="*" />
                        </Grid.RowDefinitions>
                        <StackPanel
                            Grid.Row="0"
                            Grid.Column="3"
                            HorizontalAlignment="Right"
                            Orientation="Horizontal">
                            <TextBlock
                                Margin="0,0,5,0"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblRate}" />
                            <ninactrl:StepperControl Value="{Binding Telescope.MovingRate, Mode=TwoWay}" />
                        </StackPanel>
                        <Button
                            Grid.Row="1"
                            Grid.Column="0"
                            Width="80"
                            Height="80"
                            HorizontalAlignment="Right">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblAbbrWest}" />
                            <i:Interaction.Triggers>
                                <i:EventTrigger EventName="PreviewStylusDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="W" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewStylusUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="W" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="W" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="W" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="W" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="W" />
                                </i:EventTrigger>
                            </i:Interaction.Triggers>
                        </Button>
                        <Button
                            Grid.Row="1"
                            Grid.Column="2"
                            Width="80"
                            Height="80"
                            HorizontalAlignment="Left">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblAbbrEast}" />
                            <i:Interaction.Triggers>
                                <i:EventTrigger EventName="PreviewStylusDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="O" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewStylusUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="O" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="O" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="O" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="O" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="O" />
                                </i:EventTrigger>
                            </i:Interaction.Triggers>
                        </Button>
                        <Button
                            Grid.Row="0"
                            Grid.Column="1"
                            Width="80"
                            Height="80"
                            VerticalAlignment="Bottom">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblAbbrNorth}" />
                            <i:Interaction.Triggers>
                                <i:EventTrigger EventName="PreviewStylusDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="N" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewStylusUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="N" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="N" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="N" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="N" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="N" />
                                </i:EventTrigger>
                            </i:Interaction.Triggers>
                        </Button>
                        <Button
                            Grid.Row="2"
                            Grid.Column="1"
                            Width="80"
                            Height="80"
                            VerticalAlignment="Top">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblAbbrSouth}" />
                            <i:Interaction.Triggers>
                                <i:EventTrigger EventName="PreviewStylusDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="S" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewStylusUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="S" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="S" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewTouchUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="S" />
                                </i:EventTrigger>

                                <i:EventTrigger EventName="PreviewMouseLeftButtonDown">
                                    <i:InvokeCommandAction Command="{Binding MoveCommand}" CommandParameter="S" />
                                </i:EventTrigger>
                                <i:EventTrigger EventName="PreviewMouseLeftButtonUp">
                                    <i:InvokeCommandAction Command="{Binding StopMoveCommand}" CommandParameter="S" />
                                </i:EventTrigger>
                            </i:Interaction.Triggers>
                        </Button>
                        <Button
                            Grid.Row="1"
                            Grid.Column="1"
                            Width="80"
                            Height="80"
                            Command="{Binding StopSlewCommand}">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblStop}" />
                        </Button>

                        <Button
                            Grid.Row="2"
                            Grid.Column="3"
                            Width="80"
                            Height="80"
                            HorizontalAlignment="Right"
                            Command="{Binding ParkCommand}"
                            Visibility="{Binding TelescopeInfo.AtPark, Converter={StaticResource InverseBoolToVisibilityConverter}}">
                            <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblPark}" />
                        </Button>
                        <Grid
                            Grid.Row="2"
                            Grid.Column="3"
                            Width="80"
                            Height="80"
                            HorizontalAlignment="Right"
                            Visibility="{Binding Telescope, Converter={StaticResource NullToVisibilityConverter}}">
                            <Button Command="{Binding UnparkCommand}" Visibility="{Binding TelescopeInfo.AtPark, Converter={StaticResource VisibilityConverter}}">
                                <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblUnpark}" />
                            </Button>
                        </Grid>
                    </Grid>
                </GroupBox>
            </Grid>
        </Grid>
    </Grid>
</UserControl>